//
//  PostQRCode.m
//  SchoolRws
//
//  Created by Patrick Law on 01/23/14.
//  Copyright (c) 2014 Patrick Law. All rights reserved.
//

#import "PostQRCode.h"
#import "SecondViewController.h"
#import "AppDelegate.h"


@interface PostQRCode ()



@end

@implementation PostQRCode

- (void)viewDidLoad {

    [super viewDidLoad];
    AppDelegate *ad = [[UIApplication sharedApplication] delegate];
    NSLog(@"URL: %@", ad.NextURL);
          
  //  NSString *fullURL = @"http://208.109.119.123:830/SR_ifCoupon.aspx";
    NSString *fullURL = ad.NextURL;
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)pbDone:(id)sender {
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [mainStoryBoard instantiateViewControllerWithIdentifier:@"StartController"];
    [self presentViewController:vc animated:YES completion:nil];

}
@end
