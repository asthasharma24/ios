//
//  ViewController.m
//  QRCodeReader
//
//  Created by Gabriel Theodoropoulos on 27/11/13.
//  Copyright (c) 2013 Gabriel Theodoropoulos. All rights reserved.
//

#import "ViewController.h"
#import "PostQRCode.h"
#import "AppDelegate.h"
#import "ThankYouViewController.h"
#import "SecondViewController.h"
#import "RestaurantsViewController.h"
#import "MapViewController.h"

@interface ViewController ()
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic) BOOL isReading;

-(BOOL)startReading;
-(void)stopReading;
-(void)loadBeepSound;



@end

NSString* uniqueIdentifier;
NSString* QRURL;

@implementation ViewController
@synthesize viewPreview;
@synthesize scanLabel;
@synthesize bottomBar;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
    // Initially make the captureSession object nil.
    _captureSession = nil;
    
    // Set the initial value of the flag to NO.
    _isReading = NO;
    
    // Begin loading the sound effect so to have it ready for playback when it's needed.
//    NSString *fullURL = @"";
//    NSURL *url = [NSURL URLWithString:fullURL];
//    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
////    [_entryPage loadRequest:requestObj];
//    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    
//    if (screenWidth == 320) {
//        self.buttonsContainer = [[UIView alloc] init];
//        self.buttonsContainer.tag = 68;
//        self.buttonsContainer.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-35, 320, 35);
//        self.buttonsContainer.backgroundColor = [UIColor clearColor];
//        
//        self.home = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 77, 35)];
//        self.home.tag = 1;
//        [self.home setBackgroundImage:[UIImage imageNamed:@"375Home.png"] forState:UIControlStateNormal];
//        [self.home addTarget:self
//                      action:@selector(newView:)
//            forControlEvents:UIControlEventTouchUpInside];
//        
//        self.restuarants = [[UIButton alloc] initWithFrame:CGRectMake(77, 0, 74, 35)];
//        self.restuarants.tag = 2;
//        [self.restuarants setBackgroundImage:[UIImage imageNamed:@"375Restaurants.png"] forState:UIControlStateNormal];
//        [self.restuarants addTarget:self
//                             action:@selector(newView:)
//                   forControlEvents:UIControlEventTouchUpInside];
//        
//        self.record = [[UIButton alloc] initWithFrame:CGRectMake(151, 0, 79, 35)];
//        self.record.tag = 3;
//        [self.record setBackgroundImage:[UIImage imageNamed:@"375Record.png"] forState:UIControlStateNormal];
//        [self.record addTarget:self
//                        action:@selector(newView:)
//              forControlEvents:UIControlEventTouchUpInside];
//        
//        self.map = [[UIButton alloc] initWithFrame:CGRectMake(230, 0, 90, 35)];
//        self.map.tag = 4;
//        [self.map setBackgroundImage:[UIImage imageNamed:@"375Map.png"] forState:UIControlStateNormal];
//        [self.map addTarget:self
//                     action:@selector(newView:)
//           forControlEvents:UIControlEventTouchUpInside];
//        
//        [self.buttonsContainer addSubview:self.home];
//        [self.buttonsContainer addSubview:self.restuarants];
//        [self.buttonsContainer addSubview:self.record];
//        [self.buttonsContainer addSubview:self.map];
//        
//        [self.view addSubview:self.buttonsContainer];
//    }
//    
//    if (screenWidth == 375) {
//        self.buttonsContainer = [[UIView alloc] init];
//        self.buttonsContainer.tag = 68;
//        self.buttonsContainer.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-41, 375, 41);
//        self.buttonsContainer.backgroundColor = [UIColor clearColor];
//        
//        self.home = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 91, 41)];
//        self.home.tag = 1;
//        [self.home setBackgroundImage:[UIImage imageNamed:@"375Home.png"] forState:UIControlStateNormal];
//        [self.home addTarget:self
//                      action:@selector(newView:)
//            forControlEvents:UIControlEventTouchUpInside];
//        
//        self.restuarants = [[UIButton alloc] initWithFrame:CGRectMake(91, 0, 86, 41)];
//        self.restuarants.tag = 2;
//        [self.restuarants setBackgroundImage:[UIImage imageNamed:@"375Restaurants.png"] forState:UIControlStateNormal];
//        [self.restuarants addTarget:self
//                             action:@selector(newView:)
//                   forControlEvents:UIControlEventTouchUpInside];
//        
//        self.record = [[UIButton alloc] initWithFrame:CGRectMake(177, 0, 93, 41)];
//        self.record.tag = 3;
//        [self.record setBackgroundImage:[UIImage imageNamed:@"375Record.png"] forState:UIControlStateNormal];
//        [self.record addTarget:self
//                        action:@selector(newView:)
//              forControlEvents:UIControlEventTouchUpInside];
//        
//        self.map = [[UIButton alloc] initWithFrame:CGRectMake(270, 0, 105, 41)];
//        self.map.tag = 4;
//        [self.map setBackgroundImage:[UIImage imageNamed:@"375Map.png"] forState:UIControlStateNormal];
//        [self.map addTarget:self
//                     action:@selector(newView:)
//           forControlEvents:UIControlEventTouchUpInside];
//        
//        [self.buttonsContainer addSubview:self.home];
//        [self.buttonsContainer addSubview:self.restuarants];
//        [self.buttonsContainer addSubview:self.record];
//        [self.buttonsContainer addSubview:self.map];
//        
//        [self.view addSubview:self.buttonsContainer];
//    }
//    
//    if (screenWidth == 414) {
//        self.buttonsContainer = [[UIView alloc] init];
//        self.buttonsContainer.tag = 68;
//        self.buttonsContainer.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-45, 414, 45);
//        self.buttonsContainer.backgroundColor = [UIColor clearColor];
//        
//        self.home = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 45)];
//        self.home.tag = 1;
//        [self.home setBackgroundImage:[UIImage imageNamed:@"414Home.png"] forState:UIControlStateNormal];
//        [self.home addTarget:self
//                      action:@selector(newView:)
//            forControlEvents:UIControlEventTouchUpInside];
//        
//        self.restuarants = [[UIButton alloc] initWithFrame:CGRectMake(100, 0, 95, 45)];
//        self.restuarants.tag = 2;
//        [self.restuarants setBackgroundImage:[UIImage imageNamed:@"414Restaurants.png"] forState:UIControlStateNormal];
//        [self.restuarants addTarget:self
//                             action:@selector(newView:)
//                   forControlEvents:UIControlEventTouchUpInside];
//        
//        self.record = [[UIButton alloc] initWithFrame:CGRectMake(195, 0, 103, 45)];
//        self.record.tag = 3;
//        [self.record setBackgroundImage:[UIImage imageNamed:@"414Record.png"] forState:UIControlStateNormal];
//        [self.record addTarget:self
//                        action:@selector(newView:)
//              forControlEvents:UIControlEventTouchUpInside];
//        
//        self.map = [[UIButton alloc] initWithFrame:CGRectMake(298, 0, 116, 45)];
//        self.map.tag = 4;
//        [self.map setBackgroundImage:[UIImage imageNamed:@"414Map.png"] forState:UIControlStateNormal];
//        [self.map addTarget:self
//                     action:@selector(newView:)
//           forControlEvents:UIControlEventTouchUpInside];
//        
//        [self.buttonsContainer addSubview:self.home];
//        [self.buttonsContainer addSubview:self.restuarants];
//        [self.buttonsContainer addSubview:self.record];
//        [self.buttonsContainer addSubview:self.map];
//        
//        [self.view addSubview:self.buttonsContainer];
//    }
//    
//    if (screenWidth > 414) {
        self.buttonsContainer = [[UIView alloc] init];
        self.buttonsContainer.tag = 68;
        self.buttonsContainer.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width - 300) / 2, [[UIScreen mainScreen] bounds].size.height-45, 300, 45);
        self.buttonsContainer.backgroundColor = [UIColor clearColor];
        
        self.home = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 45)];
        self.home.tag = 1;
        [self.home setBackgroundImage:[UIImage imageNamed:@"414Home.png"] forState:UIControlStateNormal];
        [self.home addTarget:self
                      action:@selector(newView:)
            forControlEvents:UIControlEventTouchUpInside];
        
        self.restuarants = [[UIButton alloc] initWithFrame:CGRectMake(100, 0, 95, 45)];
        self.restuarants.tag = 2;
        [self.restuarants setBackgroundImage:[UIImage imageNamed:@"414Restaurants.png"] forState:UIControlStateNormal];
        [self.restuarants addTarget:self
                             action:@selector(newView:)
                   forControlEvents:UIControlEventTouchUpInside];
        
        self.record = [[UIButton alloc] initWithFrame:CGRectMake(195, 0, 103, 45)];
        self.record.tag = 3;
        [self.record setBackgroundImage:[UIImage imageNamed:@"414Record.png"] forState:UIControlStateNormal];
        [self.record addTarget:self
                        action:@selector(newView:)
              forControlEvents:UIControlEventTouchUpInside];
    
        self.map = [[UIButton alloc] initWithFrame:CGRectMake(195, 0, 103, 45)];
//        self.map = [[UIButton alloc] initWithFrame:CGRectMake(298, 0, 116, 45)];
        self.map.tag = 4;
        [self.map setBackgroundImage:[UIImage imageNamed:@"414Map.png"] forState:UIControlStateNormal];
        [self.map addTarget:self
                     action:@selector(newView:)
           forControlEvents:UIControlEventTouchUpInside];
        
        [self.buttonsContainer addSubview:self.home];
        [self.buttonsContainer addSubview:self.restuarants];
//        [self.buttonsContainer addSubview:self.record];
        [self.buttonsContainer addSubview:self.map];
        
        [self.view addSubview:self.buttonsContainer];
//    }


    [self loadBeepSound];
//    [self startReading];
}

- (void) newView: (id) sender {
    UIButton *clicked = (UIButton *) sender;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if (clicked.tag == 1) {
        SecondViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SecondViewController"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    if (clicked.tag == 2) {
        RestaurantsViewController *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"RestaurantsViewController"];
        [self presentViewController:vc1 animated:NO completion:nil];
    }
    if (clicked.tag == 3) {
        ViewController *vc2 = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [self presentViewController:vc2 animated:NO completion:nil];
    }
    if (clicked.tag == 4) {
        MapViewController *vc3 = [storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
        [self presentViewController:vc3 animated:NO completion:nil];
    }
}

- (void)viewDidLayoutSubviews {
    [self startReading];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - IBAction method implementation

- (IBAction)startStopReading:(id)sender {
    if (!_isReading) {
        // This is the case where the app should read a QR code when the start button is tapped.
        if ([self startReading]) {
            // If the startReading methods returns YES and the capture session is successfully
            // running, then change the start button title and the status message.
            [_bbitemStart setTitle:@"  Stop"];
            [_lblStatus setText:@"Scanning for QR Code..."];
        }
    }
    else{
        // In this case the app is currently reading a QR code and it should stop doing so.
        [self stopReading];
        // The bar button item's title should change again.
        [_bbitemStart setTitle:@"  Start"];
    }
    
    // Set to the flag the exact opposite value of the one that currently has.
    _isReading = !_isReading;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    // nothing to do here
}

#pragma mark - Private method implementation

- (BOOL)startReading {
    NSError *error;
    
//    NSString *fullURL = @"";
//    NSURL *url = [NSURL URLWithString:fullURL];
//    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//    [_entryPage loadRequest:requestObj];
   
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];

    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    // Initialize the captureSession object.
    _captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [_captureSession addInput:input];
    
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
//    NSLog(_viewPreview.layer.bounds);
    [_videoPreviewLayer setFrame:viewPreview.layer.bounds];
    [viewPreview.layer addSublayer:_videoPreviewLayer];
    for (UIView *subview in self.view.subviews) {
        NSLog(@"subviews=%@ tag=%i",subview, subview.tag);
    }
    [self.viewPreview bringSubviewToFront:self.scanLabel];
    [self.viewPreview bringSubviewToFront:self.bottomBar];
    
    // Start video capture.
    [_captureSession startRunning];
    
    return YES;
}


-(void)stopReading{
    // Stop video capture and make the capture session object nil.
    [_captureSession stopRunning];
    _captureSession = nil;
    
    // Remove the video preview layer from the viewPreview view's layer.
    [_videoPreviewLayer removeFromSuperlayer];

}


-(void)loadBeepSound{
    // Get the path to the beep.mp3 file and convert it to a NSURL object.
    NSString *beepFilePath = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
    NSURL *beepURL = [NSURL URLWithString:beepFilePath];

    NSError *error;
    
    // Initialize the audio player object using the NSURL object previously set.
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:beepURL error:&error];
    if (error) {
        // If the audio player cannot be initialized then log a message.
        NSLog(@"Could not play beep file.");
        NSLog(@"%@", [error localizedDescription]);
    }
    else{
        // If the audio player was successfully initialized then load it in memory.
        [_audioPlayer prepareToPlay];
    }
}


#pragma mark - AVCaptureMetadataOutputObjectsDelegate method implementation

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        // Get the metadata object.
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            NSString *fullURL = [NSString stringWithFormat:@"%@&DeviceID=%@", metadataObj.stringValue, uniqueIdentifier];
            ThankYouViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ThankYou"];
            NSLog(@"QR LINK: %@", fullURL);
            vc.fullUrl = fullURL;

            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:vc animated:NO completion:nil];
            });
            _isReading = NO;
            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
             
                
            }
        }
    }
}

@end
