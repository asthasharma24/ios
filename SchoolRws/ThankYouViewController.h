//
//  ThankYouViewController.h
//  SchoolRws
//
//  Created by Patrick Law on 01/23/14.
//  Copyright (c) 2014 Patrick Law. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThankYouViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, copy) NSString *fullUrl;
@property (strong, nonatomic) IBOutlet UIView *buttonsContainer;
@property (strong, nonatomic) UIButton *home;
@property (strong, nonatomic) UIButton *restuarants;
@property (strong, nonatomic) UIButton *map;
@property (strong, nonatomic) UIButton *toolBar;
@property (strong, nonatomic) UIButton *record;
@end
