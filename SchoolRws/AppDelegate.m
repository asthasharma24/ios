//
//  AppDelegate.m
//  SchoolRws
//
//  Created by Patrick Law on 01/23/14.
//  Copyright (c) 2014 Patrick Law. All rights reserved.
//

#import "AppDelegate.h"
#import "SecondViewController.h"
#import "RestaurantsViewController.h"
#import "ViewController.h"
#import "MapViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize xyzzy;
@synthesize DeviceID;
@synthesize NextURL;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self checkForWIFIConnection];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        [application setStatusBarStyle:UIStatusBarStyleLightContent];
        self.window.clipsToBounds =YES;
        self.window.frame =  CGRectMake(0,20,self.window.frame.size.width,self.window.frame.size.height-20);
    }
    
    [Gimbal setAPIKey:@"efc75a54-504a-4360-8091-95bfe5ec39ff" options:nil];
    
    [GMBLPlaceManager startMonitoring];
    [GMBLCommunicationManager startReceivingCommunications];
    
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey])
    {
        [self processRemoteNotification:[launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]];
    }
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey])
    {
        [self processLocalNotification:[launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey]];
    }
    
    [self registerForNotifications:application];
    [self detectBluetooth];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SecondViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SecondViewController"];
//    RestaurantsViewController *vc2 = [storyboard instantiateViewControllerWithIdentifier:@"RestaurantsViewController"];
//    ViewController *vc3 = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//    MapViewController *vc4 = [storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
//    NSArray* controllers = [NSArray arrayWithObjects:vc, vc2, vc3, vc4, nil];
//    self.tabBarController.viewControllers = controllers;
    
//    self.window.rootViewController = self.tabBarController;
    self.window.rootViewController = vc;
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)checkForWIFIConnection {
    Reachability* netReach = [Reachability reachabilityForInternetConnection];
    Reachability* wifiReach = [Reachability reachabilityForLocalWiFi];
    NetworkStatus netStatus = [netReach currentReachabilityStatus];
    NetworkStatus netStatus2 = [wifiReach currentReachabilityStatus];
    if (netStatus2!=ReachableViaWiFi && netStatus!=ReachableViaWWAN)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No Internet Connection available!", @"AlertView")
            message:NSLocalizedString(@"You have no internet connection available. Please connect to a network.", @"AlertView")
                delegate:self
                cancelButtonTitle:NSLocalizedString(@"OK", @"AlertView")
                otherButtonTitles:nil, nil]; //NSLocalizedString(@"", @"AlertView")
        [alertView show];
        
    }
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
# pragma mark - Remote Notification Support

# pragma mark - Remote Notification Support

- (void)registerForNotifications:(UIApplication *)application
{
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        UIUserNotificationType types = UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    else
    {
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [Gimbal setPushDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Registration for remote notifications failed with error %@", error.description);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [self processRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [self processLocalNotification:notification];
}

- (void)processRemoteNotification:(NSDictionary *)userInfo
{
    GMBLCommunication *communication = [GMBLCommunicationManager communicationForRemoteNotification:userInfo];
    
    if (communication)
    {
        [self storeCommunication:communication];
    }
}

- (void)processLocalNotification:(UILocalNotification *)notification
{
    GMBLCommunication *communication = [GMBLCommunicationManager communicationForLocalNotification:notification];
    
    if (communication)
    {
      
        NSString *rid;
        if([communication.attributes stringForKey:@"rid"]!=NULL)
        {
            rid =[communication.attributes stringForKey:@"rid"];
        }
        else{
            rid = @"";
        }
        
        [[NSUserDefaults standardUserDefaults] setValue:rid forKey:@"rid"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if ([[communication.attributes stringForKey:@"Event"] isEqualToString:@"Exit"]) {
             [[NSNotificationCenter defaultCenter]
              postNotificationName:@"exitCommunicationReceived"
              object:communication];
         }
        else{
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"enterCommunicationReceived"
             object:communication];
        }
        [[UIApplication sharedApplication] cancelLocalNotification:notification];
        [self storeCommunication:communication];
    }
}

- (void)storeCommunication:(GMBLCommunication *)communication
{
    
    SecondViewController *vc = [[SecondViewController alloc] init];
    [vc addCommunication:communication];
}


#pragma make bluetooth detection

- (void)detectBluetooth
{
    if(!self.bluetoothManager)
    {
        // Put on main queue so we can call UIAlertView from delegate callbacks.
        self.bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
    }
    [self centralManagerDidUpdateState:self.bluetoothManager]; // Show initial state
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSString *stateString = nil;
    switch(central.state)
    {
        case CBCentralManagerStateResetting: stateString = @"The connection with the system service was momentarily lost, update imminent."; break;
        case CBCentralManagerStateUnsupported: stateString = @"The platform doesn't support Bluetooth Low Energy."; break;
        case CBCentralManagerStateUnauthorized: stateString = @"The app is not authorized to use Bluetooth Low Energy."; break;
        case CBCentralManagerStatePoweredOff:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Schoolicious" message:@"Please turn on Bluetooth to use this application." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
            break;
        case CBCentralManagerStatePoweredOn: stateString = @"Bluetooth is currently powered on and available to use."; break;
        default: stateString = @"State unknown, update imminent."; break;
    }
}

@end
