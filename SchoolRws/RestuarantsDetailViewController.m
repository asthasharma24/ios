//
//  RestuarantsDetailViewController.m
//  SchoolRws
//
//  Created by Patrick Law on 01/23/14.
//  Copyright (c) 2014 Patrick Law. All rights reserved.
//

#import "RestuarantsDetailViewController.h"
#import "SecondViewController.h"
#import "RestaurantsViewController.h"
#import "ViewController.h"
#import "MapViewController.h"

@interface RestuarantsDetailViewController ()

@end

@implementation RestuarantsDetailViewController
@synthesize webView, fullUrl;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSURL *url = [NSURL URLWithString:self.fullUrl];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
//    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    
//    if (screenWidth == 320) {
//        self.buttonsContainer = [[UIView alloc] init];
//        self.buttonsContainer.tag = 68;
//        self.buttonsContainer.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-35, 320, 35);
//        self.buttonsContainer.backgroundColor = [UIColor clearColor];
//        
//        self.home = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 77, 35)];
//        self.home.tag = 1;
//        [self.home setBackgroundImage:[UIImage imageNamed:@"375Home.png"] forState:UIControlStateNormal];
//        [self.home addTarget:self
//                      action:@selector(newView:)
//            forControlEvents:UIControlEventTouchUpInside];
//        
//        self.restuarants = [[UIButton alloc] initWithFrame:CGRectMake(77, 0, 74, 35)];
//        self.restuarants.tag = 2;
//        [self.restuarants setBackgroundImage:[UIImage imageNamed:@"375Restaurants.png"] forState:UIControlStateNormal];
//        [self.restuarants addTarget:self
//                             action:@selector(newView:)
//                   forControlEvents:UIControlEventTouchUpInside];
//        
//        self.record = [[UIButton alloc] initWithFrame:CGRectMake(151, 0, 79, 35)];
//        self.record.tag = 3;
//        [self.record setBackgroundImage:[UIImage imageNamed:@"375Record.png"] forState:UIControlStateNormal];
//        [self.record addTarget:self
//                        action:@selector(newView:)
//              forControlEvents:UIControlEventTouchUpInside];
//        
//        self.map = [[UIButton alloc] initWithFrame:CGRectMake(230, 0, 90, 35)];
//        self.map.tag = 4;
//        [self.map setBackgroundImage:[UIImage imageNamed:@"375Map.png"] forState:UIControlStateNormal];
//        [self.map addTarget:self
//                     action:@selector(newView:)
//           forControlEvents:UIControlEventTouchUpInside];
//        
//        [self.buttonsContainer addSubview:self.home];
//        [self.buttonsContainer addSubview:self.restuarants];
//        [self.buttonsContainer addSubview:self.record];
//        [self.buttonsContainer addSubview:self.map];
//        
//        [self.view addSubview:self.buttonsContainer];
//    }
//    
//    if (screenWidth == 375) {
//        self.buttonsContainer = [[UIView alloc] init];
//        self.buttonsContainer.tag = 68;
//        self.buttonsContainer.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-41, 375, 41);
//        self.buttonsContainer.backgroundColor = [UIColor clearColor];
//        
//        self.home = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 91, 41)];
//        self.home.tag = 1;
//        [self.home setBackgroundImage:[UIImage imageNamed:@"375Home.png"] forState:UIControlStateNormal];
//        [self.home addTarget:self
//                      action:@selector(newView:)
//            forControlEvents:UIControlEventTouchUpInside];
//        
//        self.restuarants = [[UIButton alloc] initWithFrame:CGRectMake(91, 0, 86, 41)];
//        self.restuarants.tag = 2;
//        [self.restuarants setBackgroundImage:[UIImage imageNamed:@"375Restaurants.png"] forState:UIControlStateNormal];
//        [self.restuarants addTarget:self
//                             action:@selector(newView:)
//                   forControlEvents:UIControlEventTouchUpInside];
//        
//        self.record = [[UIButton alloc] initWithFrame:CGRectMake(177, 0, 93, 41)];
//        self.record.tag = 3;
//        [self.record setBackgroundImage:[UIImage imageNamed:@"375Record.png"] forState:UIControlStateNormal];
//        [self.record addTarget:self
//                        action:@selector(newView:)
//              forControlEvents:UIControlEventTouchUpInside];
//        
//        self.map = [[UIButton alloc] initWithFrame:CGRectMake(270, 0, 105, 41)];
//        self.map.tag = 4;
//        [self.map setBackgroundImage:[UIImage imageNamed:@"375Map.png"] forState:UIControlStateNormal];
//        [self.map addTarget:self
//                     action:@selector(newView:)
//           forControlEvents:UIControlEventTouchUpInside];
//        
//        [self.buttonsContainer addSubview:self.home];
//        [self.buttonsContainer addSubview:self.restuarants];
//        [self.buttonsContainer addSubview:self.record];
//        [self.buttonsContainer addSubview:self.map];
//        
//        [self.view addSubview:self.buttonsContainer];
//    }
//    
//    if (screenWidth == 414) {
//        self.buttonsContainer = [[UIView alloc] init];
//        self.buttonsContainer.tag = 68;
//        self.buttonsContainer.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-45, 414, 45);
//        self.buttonsContainer.backgroundColor = [UIColor clearColor];
//        
//        self.home = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 45)];
//        self.home.tag = 1;
//        [self.home setBackgroundImage:[UIImage imageNamed:@"414Home.png"] forState:UIControlStateNormal];
//        [self.home addTarget:self
//                      action:@selector(newView:)
//            forControlEvents:UIControlEventTouchUpInside];
//        
//        self.restuarants = [[UIButton alloc] initWithFrame:CGRectMake(100, 0, 95, 45)];
//        self.restuarants.tag = 2;
//        [self.restuarants setBackgroundImage:[UIImage imageNamed:@"414Restaurants.png"] forState:UIControlStateNormal];
//        [self.restuarants addTarget:self
//                             action:@selector(newView:)
//                   forControlEvents:UIControlEventTouchUpInside];
//        
//        self.record = [[UIButton alloc] initWithFrame:CGRectMake(195, 0, 103, 45)];
//        self.record.tag = 3;
//        [self.record setBackgroundImage:[UIImage imageNamed:@"414Record.png"] forState:UIControlStateNormal];
//        [self.record addTarget:self
//                        action:@selector(newView:)
//              forControlEvents:UIControlEventTouchUpInside];
//        
//        self.map = [[UIButton alloc] initWithFrame:CGRectMake(298, 0, 116, 45)];
//        self.map.tag = 4;
//        [self.map setBackgroundImage:[UIImage imageNamed:@"414Map.png"] forState:UIControlStateNormal];
//        [self.map addTarget:self
//                     action:@selector(newView:)
//           forControlEvents:UIControlEventTouchUpInside];
//        
//        [self.buttonsContainer addSubview:self.home];
//        [self.buttonsContainer addSubview:self.restuarants];
//        [self.buttonsContainer addSubview:self.record];
//        [self.buttonsContainer addSubview:self.map];
//        
//        [self.view addSubview:self.buttonsContainer];
//    }
//    
//    if (screenWidth > 414) {
        self.buttonsContainer = [[UIView alloc] init];
        self.buttonsContainer.tag = 68;
        self.buttonsContainer.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width - 300) / 2, [[UIScreen mainScreen] bounds].size.height-45, 300, 45);
        self.buttonsContainer.backgroundColor = [UIColor clearColor];
        
        self.home = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 45)];
        self.home.tag = 1;
        [self.home setBackgroundImage:[UIImage imageNamed:@"414Home.png"] forState:UIControlStateNormal];
        [self.home addTarget:self
                      action:@selector(newView:)
            forControlEvents:UIControlEventTouchUpInside];
        
        self.restuarants = [[UIButton alloc] initWithFrame:CGRectMake(100, 0, 95, 45)];
        self.restuarants.tag = 2;
        [self.restuarants setBackgroundImage:[UIImage imageNamed:@"414Restaurants.png"] forState:UIControlStateNormal];
        [self.restuarants addTarget:self
                             action:@selector(newView:)
                   forControlEvents:UIControlEventTouchUpInside];
        
        self.record = [[UIButton alloc] initWithFrame:CGRectMake(195, 0, 103, 45)];
        self.record.tag = 3;
        [self.record setBackgroundImage:[UIImage imageNamed:@"414Record.png"] forState:UIControlStateNormal];
        [self.record addTarget:self
                        action:@selector(newView:)
              forControlEvents:UIControlEventTouchUpInside];
    
    self.map = [[UIButton alloc] initWithFrame:CGRectMake(195, 0, 103, 45)];
//        self.map = [[UIButton alloc] initWithFrame:CGRectMake(298, 0, 116, 45)];
        self.map.tag = 4;
        [self.map setBackgroundImage:[UIImage imageNamed:@"414Map.png"] forState:UIControlStateNormal];
        [self.map addTarget:self
                     action:@selector(newView:)
           forControlEvents:UIControlEventTouchUpInside];
        
        [self.buttonsContainer addSubview:self.home];
        [self.buttonsContainer addSubview:self.restuarants];
//        [self.buttonsContainer addSubview:self.record];
        [self.buttonsContainer addSubview:self.map];
        
        [self.view addSubview:self.buttonsContainer];
//    }
    
    [self.webView loadRequest:requestObj];
}

- (void) newView: (id) sender {
    UIButton *clicked = (UIButton *) sender;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if (clicked.tag == 1) {
        SecondViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SecondViewController"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    if (clicked.tag == 2) {
        RestaurantsViewController *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"RestaurantsViewController"];
        [self presentViewController:vc1 animated:NO completion:nil];
    }
    if (clicked.tag == 3) {
        ViewController *vc2 = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [self presentViewController:vc2 animated:NO completion:nil];
    }
    if (clicked.tag == 4) {
        MapViewController *vc3 = [storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
        [self presentViewController:vc3 animated:NO completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
