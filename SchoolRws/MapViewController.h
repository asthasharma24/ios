//
//  MapViewController.h
//  SchoolRws
//
//  Created by Patrick Law on 01/23/14.
//  Copyright (c) 2014 Patrick Law. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapKit/MapKit.h"
//- (void)first:(NSString *)fname second:(NSString *)mname third:(NSString *)lname;


@interface MapViewController : UIViewController <MKMapViewDelegate>
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) IBOutlet UILabel *greetingId;
@property (nonatomic, strong) IBOutlet UILabel *greetingContent;
@property (weak, nonatomic) IBOutlet UISwitch *WhichView;

- (void) AddAnnotation:(double)lat second:(double)lng  third:(NSString *)title  fourth:(NSString *)subtitle;
- (IBAction)SwitchViews:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *mapButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *recordMealButton;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;

- (IBAction)removeWebViewToShowMap:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *buttonsContainer;
@property (strong, nonatomic) UIButton *home;
@property (strong, nonatomic) UIButton *restuarants;
@property (strong, nonatomic) UIButton *map;
@property (strong, nonatomic) UIButton *toolBar;
@property (strong, nonatomic) UIButton *record;

@end
