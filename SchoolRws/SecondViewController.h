//
//  SecondViewController.h
//  SchoolRws
//
//  Created by Patrick Law on 01/23/14.
//  Copyright (c) 2014 Patrick Law. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Gimbal/Gimbal.h>

@interface SecondViewController : UIViewController <UIWebViewDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) UIWebView *viewWeb;
@property (strong, nonatomic) IBOutlet UIView *buttonsContainer;
@property (strong, nonatomic) IBOutlet UIView *homeButtonsContainer;

@property (strong, nonatomic) UIButton *about;
@property (strong, nonatomic) UIButton *terms;
@property (strong, nonatomic) UIButton *contact;

@property (strong, nonatomic) UIButton *profile;

@property (strong, nonatomic) UIButton *home;
@property (strong, nonatomic) UIButton *blank;
@property (strong, nonatomic) UIButton *blank_end;
@property (strong, nonatomic) UIButton *restuarants;
@property (strong, nonatomic) UIButton *map;
@property (strong, nonatomic) UIButton *toolBar;
@property (strong, nonatomic) UIButton *record;
@property (weak, nonatomic) IBOutlet UIWebView *qaViewWeb;
- (IBAction)headerHomeBtnTapped:(id)sender;
- (void)addCommunication:(GMBLCommunication *)communication;

@end

