//
//  SecondViewController.m
//  SchoolRws
//
//  Created by Patrick Law on 01/23/14.
//  Copyright (c) 2014 Patrick Law. All rights reserved.
//

#import "SecondViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "RestaurantsViewController.h"
#import "MapViewController.h"
#import "QuestionAnswersViewController.h"

@interface SecondViewController ()<GMBLPlaceManagerDelegate, GMBLCommunicationManagerDelegate>
@property (nonatomic) GMBLPlaceManager *placeManager;
@property (nonatomic) GMBLCommunicationManager *communicationManager;


@end

@implementation SecondViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(exitCommunicationReceived:)
                                                 name:@"exitCommunicationReceived"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enterCommunicationReceived:)
                                                 name:@"enterCommunicationReceived"
                                               object:nil];
    
    
    
    [self setNeedsStatusBarAppearanceUpdate];
    
   
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)exitCommunicationReceived:(NSNotification *)noti
{
    NSLog(@"notifiaction receieved..");
    NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
     NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://app.schoolicious.org/participatingrestaurant?rid=%@&DeviceID=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"rid"],uniqueIdentifier]];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_viewWeb loadRequest:requestObj];
    
}

- (void)enterCommunicationReceived:(NSNotification *)noti
{
    NSLog(@"notifiaction receieved..");
    NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://app.schoolicious.org/SR_Menu?RID=%@&DeviceID=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"rid"],uniqueIdentifier]];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_viewWeb loadRequest:requestObj];
    
}

- (void) newView: (id) sender {
    UIButton *clicked = (UIButton *) sender;
 
    NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
 
    
    if (clicked.tag == 1) {
   
        // load page for About
        
        NSURL *url = [NSURL URLWithString:@"https://app.schoolicious.org/sr_about"];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [_viewWeb loadRequest:requestObj];
    }
    if (clicked.tag == 2) { // terms
        
        NSURL *url = [NSURL URLWithString:@"https://app.schoolicious.org/sr_TermsConditions"];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [_viewWeb loadRequest:requestObj];
        
    }
    if (clicked.tag == 3) { // contact
        
        NSURL *url = [NSURL URLWithString:@"https://app.schoolicious.org/sr_Contact"];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [_viewWeb loadRequest:requestObj];
       
    }
    if (clicked.tag == 4) { // home
       
        NSString *fullURL = [NSString stringWithFormat:@"https://app.schoolicious.org/SR_Home?DeviceID=%@",uniqueIdentifier];
        
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [_viewWeb loadRequest:requestObj];
        
    }
    if (clicked.tag == 5) { // restaurant
       
        NSString *fullURL = [NSString stringWithFormat:@"https://app.schoolicious.org/SR_RList.aspx?DeviceID=%@",uniqueIdentifier];
        
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [_viewWeb loadRequest:requestObj];
        
    }
    if (clicked.tag == 6) { // profile
        
        NSString *fullURL = [NSString stringWithFormat:@"https://app.schoolicious.org/SR_Profile.aspx?DeviceID=%@",uniqueIdentifier];
        
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [_viewWeb loadRequest:requestObj];
        
    }
}

- (void)viewWillLayoutSubviews {
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    if (screenWidth == 320) {
        
//        self.buttonsContainer = [[UIView alloc] init];
//        self.buttonsContainer.tag = 68;
//        self.buttonsContainer.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-50, 320, 50);
//        self.buttonsContainer.backgroundColor = [UIColor clearColor];
//        
//        self.home = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320/4, 50)];
//        [self.home setBackgroundImage:[UIImage imageNamed:@"home320.png"] forState:UIControlStateNormal];
//        
//        self.restuarants = [[UIButton alloc] initWithFrame:CGRectMake(0, 80, 320/4, 50)];
//        [self.restuarants setBackgroundImage:[UIImage imageNamed:@"restaruants320.png"] forState:UIControlStateNormal];
//        
//        self.record = [[UIButton alloc] initWithFrame:CGRectMake(0, 160, 320/4, 50)];
//        [self.record setBackgroundImage:[UIImage imageNamed:@"record320.png"] forState:UIControlStateNormal];
//        
//        self.map = [[UIButton alloc] initWithFrame:CGRectMake(0, 240, 320/4, 50)];
//        [self.record setBackgroundImage:[UIImage imageNamed:@"map320.png"] forState:UIControlStateNormal];
//        
//        [self.buttonsContainer addSubview:self.home];
//        [self.buttonsContainer addSubview:self.restuarants];
//        [self.buttonsContainer addSubview:self.record];
//        [self.buttonsContainer addSubview:self.map];
//        
//        [self.view addSubview:self.buttonsContainer];

        
        
      
//        self.buttonsContainer.frame = CGRectMake([[UIScreen mainScreen] bounds].size.height - 50, 10, 300, 50);
    }
    if (screenWidth == 375) {
//        self.toolBar = [[UIToolbar alloc] init];
//        self.toolBar.tag = 68;
//        self.toolBar.frame = CGRectMake([[UIScreen mainScreen] bounds].size.height - 50, 10, 300, 50);
//        self.toolBar.backgroundColor = [UIColor blackColor];
//        UIBarButtonItem *flexiableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
//        
//        self.home = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"buttons_01.png"] style:UIBarButtonItemStyleDone target:self action:@selector(pushNewView:)];
//        self.home.tag = 69;
//        self.restuarants = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"buttons_02.png"] style:UIBarButtonItemStyleDone target:self action:@selector(pushNewView:)];
//        self.restuarants.tag = 71;
//        self.record = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"buttons_03.png"] style:UIBarButtonItemStyleDone target:self action:@selector(pushNewView:)];
//        self.record.tag = 72;
//        self.map = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"buttons_04.png"] style:UIBarButtonItemStyleDone target:self action:@selector(pushNewView:)];
//        self.map.tag = 71;
//        NSMutableArray *items = [[NSMutableArray alloc] initWithObjects:flexiableItem, self.home, self.restuarants, self.record, self.map, flexiableItem, nil];
//        [self.toolBar setItems:items];
//        [[self view] addSubview:self.toolBar];
//        self.buttonsContainer.frame = CGRectMake([[UIScreen mainScreen] bounds].size.height - 50, 10, 355, 50);
    }
}

- (void)pushNewView {
    
}

- (void)viewWillAppear:(BOOL)animated{
    self.buttonsContainer.hidden = YES;
   
    self.viewWeb.delegate = self;
    [self removeButton];
    [self addButton];
    NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSLog(@"Unique identifier: %@", uniqueIdentifier);
    NSString *fullURL = [NSString stringWithFormat:@"https://app.schoolicious.org/SR_TermsCondition?DeviceID=%@", uniqueIdentifier];
//    NSString *fullURL = [NSString stringWithFormat:@"https://app.schoolicious.org/SR_Home?DeviceID=%@",uniqueIdentifier];
   
    //    NSString *fullURL = [NSString  :@"http://www.shiningstartherapy.com/pediatric-therapy-jobs/"];
    
    CGFloat tabbar_height;
    
    tabbar_height = [[UIScreen mainScreen] bounds].size.height/15;
    CGFloat totalButtonSize = (tabbar_height*2)+20;
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    CGFloat startpointForPNG = (screenWidth - totalButtonSize)/2 ;
    
    CGFloat icon_width = [[UIScreen mainScreen] bounds].size.width/6;
    CGFloat totalHomeButtonSize = (icon_width*3)+20;
    CGFloat startingpointForHomeButtons = (screenWidth-totalHomeButtonSize)/2;

    
    [self.tabBarController.tabBar setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, tabbar_height)];
    
    // creation of home button container //
    
    self.homeButtonsContainer = [[UIView alloc] init];
    self.homeButtonsContainer.tag = 68;
    self.homeButtonsContainer.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-tabbar_height-20, [[UIScreen mainScreen] bounds].size.width, tabbar_height);
    self.homeButtonsContainer.backgroundColor = [UIColor colorWithRed:255/255.0f green:110/255.0f blue:38/255.0f alpha:1.0f];
    
//    self.blank = [[UIButton alloc] initWithFrame:CGRectMake(startingpointForHomeButtons, 0, 100, tabbar_height)];
//    self.blank.tag = 0;
//    [self.blank setBackgroundImage:[UIImage imageNamed:@"blank3.png"] forState:UIControlStateNormal];
    
    self.about = [[UIButton alloc] initWithFrame:CGRectMake(startingpointForHomeButtons, 0, icon_width, tabbar_height)];
    self.about.tag = 1;
    [self.about setBackgroundImage:[UIImage imageNamed:@"about3.png"] forState:UIControlStateNormal];
    [self.about addTarget:self action:@selector(newView:) forControlEvents:UIControlEventTouchUpInside];
    
    self.terms = [[UIButton alloc] initWithFrame:CGRectMake(startingpointForHomeButtons+icon_width, 0, icon_width, tabbar_height)];
    self.terms.tag = 2;
    [self.terms setBackgroundImage:[UIImage imageNamed:@"terms3.png"] forState:UIControlStateNormal];
    [self.terms addTarget:self action:@selector(newView:) forControlEvents:UIControlEventTouchUpInside];
    
    self.contact = [[UIButton alloc] initWithFrame:CGRectMake(startingpointForHomeButtons+icon_width+icon_width, 0, icon_width, tabbar_height)];
    self.contact.tag = 3;
    [self.contact setBackgroundImage:[UIImage imageNamed:@"contact3.png"] forState:UIControlStateNormal];
    [self.contact addTarget:self action:@selector(newView:) forControlEvents:UIControlEventTouchUpInside];
    
    
//    self.blank_end = [[UIButton alloc] initWithFrame:CGRectMake(4 * [[UIScreen mainScreen] bounds].size.width/5, 0, [[UIScreen mainScreen] bounds].size.width/5, tabbar_height)];
//    self.blank_end.tag = 0;
//    [self.blank_end setBackgroundImage:[UIImage imageNamed:@"blank3.png"] forState:UIControlStateNormal];
//    
    
//    [self.homeButtonsContainer addSubview:self.blank];
    [self.homeButtonsContainer addSubview:self.about];
    [self.homeButtonsContainer addSubview:self.terms];
    [self.homeButtonsContainer addSubview:self.contact];
//    [self.homeButtonsContainer addSubview:self.blank_end];
    [self.view addSubview:self.homeButtonsContainer];
    [self.homeButtonsContainer setHidden:YES];
    
    // button container //
    
    self.buttonsContainer = [[UIView alloc] init];
    self.buttonsContainer.tag = 68;
    self.buttonsContainer.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-tabbar_height-20, [[UIScreen mainScreen] bounds].size.width, tabbar_height);
    self.buttonsContainer.backgroundColor = [UIColor colorWithRed:223/255.0f green:31/255.0f blue:38/255.0f alpha:1.0f];
    
    
//    self.home = [[UIButton alloc] initWithFrame:CGRectMake( startpointForPNG, 0, tabbar_height, tabbar_height)];
//    self.home.tag = 4;
//    [self.home setBackgroundImage:[UIImage imageNamed:@"home.png"] forState:UIControlStateNormal];
//    [self.home addTarget:self action:@selector(newView:) forControlEvents:UIControlEventTouchUpInside];
    
    self.restuarants = [[UIButton alloc] initWithFrame:CGRectMake(startpointForPNG, 0, tabbar_height, tabbar_height)];
    self.restuarants.tag = 5;
    [self.restuarants setBackgroundImage:[UIImage imageNamed:@"restaurant.png"] forState:UIControlStateNormal];
    [self.restuarants addTarget:self action:@selector(newView:) forControlEvents:UIControlEventTouchUpInside];
    
    self.profile = [[UIButton alloc] initWithFrame:CGRectMake(startpointForPNG+tabbar_height+10, 0, tabbar_height, tabbar_height)];
    self.profile.tag = 6;
    [self.profile setBackgroundImage:[UIImage imageNamed:@"profile.png"] forState:UIControlStateNormal];
    [self.profile addTarget:self action:@selector(newView:) forControlEvents:UIControlEventTouchUpInside];
    
//    [self.buttonsContainer addSubview:self.home];
    [self.buttonsContainer addSubview:self.restuarants];
    [self.buttonsContainer addSubview:self.profile];
    [self.view addSubview:self.buttonsContainer];
    [self.buttonsContainer setHidden:YES];
    
    NSLog(@"%@", fullURL);
    NSURL *url = [NSURL URLWithString:fullURL];
    
    
    _viewWeb = [[UIWebView alloc] initWithFrame:CGRectMake(0, 75, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height-tabbar_height-95)];
    [_viewWeb setDelegate:self];
    
    [self.view addSubview:_viewWeb];
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_viewWeb loadRequest:requestObj];
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {


    NSLog(@"url reuqested - %@",request.URL.path);
    
    if ([request.URL.path rangeOfString:@"SR_TermsCondition"].length != 0) {
        self.homeButtonsContainer.hidden = YES;
        self.buttonsContainer.hidden = YES;
    }
    else if ([request.URL.path rangeOfString:@"Register"].length != 0) {
        self.homeButtonsContainer.hidden = YES;
        self.buttonsContainer.hidden = YES;
    }
    else if ([request.URL.path rangeOfString:@"Home"].length != 0 || [request.URL.path isEqualToString:@"/"]) {
        self.homeButtonsContainer.hidden = NO;
        self.buttonsContainer.hidden = YES;
        
        self.placeManager = [GMBLPlaceManager new];
        self.placeManager.delegate = self;
        
        self.communicationManager = [GMBLCommunicationManager new];
        self.communicationManager.delegate = self;
        
    }
    else{
        self.buttonsContainer.hidden = NO;
        self.homeButtonsContainer.hidden = YES;
        
    }
    

    return YES;
}


-(void)removeButton{

    self.buttonsContainer.hidden = YES;
}

-(void)addButton{
    self.buttonsContainer.hidden = NO;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    [_viewWeb reload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - Gimbal PlaceManager delegate methods

- (void)placeManager:(GMBLPlaceManager *)manager didBeginVisit:(GMBLVisit *)visit
{
    NSString *str = [NSString stringWithFormat:@"Entered: %@",visit.place.name];
    [self addEventWithMessage:str date:visit.arrivalDate];
}

- (void)placeManager:(GMBLPlaceManager *)manager didEndVisit:(GMBLVisit *)visit
{
    NSString *str = [NSString stringWithFormat:@"Exist: %@",visit.place.name];
    [self addEventWithMessage:str date:visit.departureDate];
}

# pragma mark - Gimbal CommunicationManager delegate methods

- (NSArray *)communicationManager:(GMBLCommunicationManager *)manager
presentLocalNotificationsForCommunications:(NSArray *)communications
                         forVisit:(GMBLVisit *)visit
{
    for (GMBLCommunication *communication in communications)
    {
        NSString *description = [NSString stringWithFormat:@"%@ %@", communication.descriptionText, @": PRESENTED"];
        [self addEventWithMessage:description date:[NSDate date]];
    }
    
    return communications;
}

#pragma mark - Utility methods

- (NSArray *)events
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"events"];
}

-(void)test{
      UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RestaurantsViewController *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"RestaurantsViewController"];
    [self presentViewController:vc1 animated:NO completion:nil];
}

- (IBAction)headerHomeBtnTapped:(id)sender {

    NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *fullURL = [NSString stringWithFormat:@"https://app.schoolicious.org/SR_TermsCondition?DeviceID=%@", uniqueIdentifier];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:fullURL]];
    [_viewWeb loadRequest:requestObj];
}

- (void)addCommunication:(GMBLCommunication *)communication
{
    NSString *description = [NSString stringWithFormat:@"%@ %@", communication.descriptionText, @": DELIVERED"];
    [self addEventWithMessage:description date:[NSDate date]];
    
    
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    
    if (state == UIApplicationStateActive && [[communication.attributes stringForKey:@"Event"] isEqualToString:@"Enter"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Schoolicious" message:communication.descriptionText delegate:self cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
        [alert show];
    
    }
   
}

// For Future use - we can store all user events with this method

- (void)addEventWithMessage:(NSString *)message date:(NSDate *)date
{
    NSDictionary *item = @{@"message":message, @"date":date};
    NSLog(@"Event %@",[item description]);
    
}



@end
