//
//  AppDelegate.h
//  SchoolRws
//
//  Created by Patrick Law on 01/23/14.
//  Copyright (c) 2014 Patrick Law. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <Gimbal/Gimbal.h>
#import <CoreBluetooth/CoreBluetooth.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate,CBCentralManagerDelegate>
{
    NSString *xyzzy;
    NSString *DeviceID;
    NSString *NextURL;
   
}

@property (nonatomic) CBCentralManager *bluetoothManager;

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) NSString *xyzzy;
@property (nonatomic, retain) NSString *DeviceID;
@property (nonatomic, retain) NSString *NextURL;


@end

