//
//  MapViewController.m
//  SchoolRws
//
//  Created by Patrick Law on 01/23/14.
//  Copyright (c) 2014 Patrick Law. All rights reserved.
//

#import "MapViewController.h"
#import "CustomMKAnnotation.h"
#import "RestuarantsDetailViewController.h"
#import "SecondViewController.h"
#import "RestaurantsViewController.h"
#import "ViewController.h"
@import CoreLocation;

#define METERS_PER_MILE 1609.344

@interface MapViewController ()<CLLocationManagerDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation MapViewController
@synthesize mapView;

- (void)viewWillAppear:(BOOL)animated {
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 32.961869;
    zoomLocation.longitude= -117.036195;
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    [mapView setRegion:viewRegion animated:YES];
    
}

// Location Manager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%@", [locations lastObject]);
}

- (void)requestAlwaysAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
        NSString *title;
        title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
        NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Settings", nil];
        [alertView show];
    }
    // The user has not enabled any location services. Request background authorization.
    else if (status == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestAlwaysAuthorization];
    }
}

- (void) AddAnnotation:(double)lat second:(double)lng  third:(NSString *)title  fourth:(NSString *)subtitle url:(NSString *)rID
{
    // Add an annotation
//    MKPointAnnotation *myAnnotation = [[MKPointAnnotation alloc] init];
//    myAnnotation.coordinate = CLLocationCoordinate2DMake(lat, lng);
//    myAnnotation.title = title;
//    myAnnotation.subtitle = subtitle;
//    [self.mapView addAnnotation:myAnnotation];
    CustomMKAnnotation *myAnnotation = [[CustomMKAnnotation alloc] init];
    myAnnotation.coordinate = CLLocationCoordinate2DMake(lat, lng);
    myAnnotation.title = title;
    //myAnnotation.subtitle = subtitle;
    myAnnotation.rID = rID;
    [self.mapView addAnnotation:myAnnotation];

}

- (IBAction)SwitchViews:(id)sender
{
    
    if(_WhichView.isOn)
    {
        mapView.mapType = MKMapTypeHybrid;
    }
    else
    {
        mapView.mapType = MKMapTypeStandard;
    }
     
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"loc"];
    annotationView.canShowCallout = YES;
    UIButton *goToDetail = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    annotationView.rightCalloutAccessoryView = goToDetail;
    //http://208.109.119.123:830/
    
 //   UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
 //   UIViewController *vc = [mainStoryBoard instantiateViewControllerWithIdentifier:@"PostQRCode"];
 //   [self presentViewController:vc animated:YES completion:nil];
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view
calloutAccessoryControlTapped:(UIControl *)control
{
    NSLog(@"was tapped");
    NSString *rID = [(CustomMKAnnotation *)[view annotation] rID];
//    NSString *fullURL = [NSString stringWithFormat:@"http://208.109.119.123:830/SR_Menu?RID=%@&RestaurantCode=%@", rID, rID];
    
    NSString *fullURL = [NSString stringWithFormat:@"https://app.schoolicious.org/SR_Menu?RID=%@&RestaurantCode=%@", rID, rID];
    
//    NSString *fullURL = @"http://208.109.119.123:830/SR_Menu?RID=96&RestaurantCode=96";

    NSLog(@"fullurl: %@", fullURL);

//    NSString *fullURL = @"http://208.109.119.123:830/SR_Menu?RID=%204259&RestaurantCode=%204259";

//    NSURL *url = [NSURL URLWithString:fullURL];
//    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//    UIWebView *restuarantsPage = [[UIWebView alloc] initWithFrame:self.view.frame];
//    restuarantsPage.tag = 555;
//    [restuarantsPage loadRequest:requestObj];
//    [self.view addSubview:restuarantsPage];
//    [self bringButtonsToFront];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RestuarantsDetailViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RestuarantsViewController"];
    vc.fullUrl = fullURL;
    [self presentViewController:vc animated:NO completion:nil];
    
}

- (void) bringButtonsToFront {
    [self.view addSubview:[self.view viewWithTag:111]];
    [self.view addSubview:[self.view viewWithTag:222]];
    [self.view addSubview:[self.view viewWithTag:333]];
}

- (IBAction)removeWebViewToShowMap:(id)sender {
    for (UIView *subview in self.view.subviews) {
        if (subview.tag == 555) {
            [subview removeFromSuperview];
        }
    }

}

/*
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
//    NSLog(@"%@",view.subviews.description.);
    NSLog(@"clicked");
}
*/

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        // Send the user to the Settings for this app
        NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:settingsURL];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView.delegate = self;
    [self requestAlwaysAuthorization];
    self.mapView.showsUserLocation = YES;
    [self loadPins];
    
//    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    
//    if (screenWidth == 320) {
//        self.buttonsContainer = [[UIView alloc] init];
//        self.buttonsContainer.tag = 68;
//        self.buttonsContainer.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-35, 320, 35);
//        self.buttonsContainer.backgroundColor = [UIColor clearColor];
//        
//        self.home = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 77, 35)];
//        self.home.tag = 1;
//        [self.home setBackgroundImage:[UIImage imageNamed:@"375Home.png"] forState:UIControlStateNormal];
//        [self.home addTarget:self
//                      action:@selector(newView:)
//            forControlEvents:UIControlEventTouchUpInside];
//        
//        self.restuarants = [[UIButton alloc] initWithFrame:CGRectMake(77, 0, 74, 35)];
//        self.restuarants.tag = 2;
//        [self.restuarants setBackgroundImage:[UIImage imageNamed:@"375Restaurants.png"] forState:UIControlStateNormal];
//        [self.restuarants addTarget:self
//                             action:@selector(newView:)
//                   forControlEvents:UIControlEventTouchUpInside];
//        
//        self.record = [[UIButton alloc] initWithFrame:CGRectMake(151, 0, 79, 35)];
//        self.record.tag = 3;
//        [self.record setBackgroundImage:[UIImage imageNamed:@"375Record.png"] forState:UIControlStateNormal];
//        [self.record addTarget:self
//                        action:@selector(newView:)
//              forControlEvents:UIControlEventTouchUpInside];
//        
//        self.map = [[UIButton alloc] initWithFrame:CGRectMake(230, 0, 90, 35)];
//        self.map.tag = 4;
//        [self.map setBackgroundImage:[UIImage imageNamed:@"375Map.png"] forState:UIControlStateNormal];
//        [self.map addTarget:self
//                     action:@selector(newView:)
//           forControlEvents:UIControlEventTouchUpInside];
//        
//        [self.buttonsContainer addSubview:self.home];
//        [self.buttonsContainer addSubview:self.restuarants];
//        [self.buttonsContainer addSubview:self.record];
//        [self.buttonsContainer addSubview:self.map];
//        
//        [self.view addSubview:self.buttonsContainer];
//    }
//    
//    if (screenWidth == 375) {
//        self.buttonsContainer = [[UIView alloc] init];
//        self.buttonsContainer.tag = 68;
//        self.buttonsContainer.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-41, 375, 41);
//        self.buttonsContainer.backgroundColor = [UIColor clearColor];
//        
//        self.home = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 91, 41)];
//        self.home.tag = 1;
//        [self.home setBackgroundImage:[UIImage imageNamed:@"375Home.png"] forState:UIControlStateNormal];
//        [self.home addTarget:self
//                      action:@selector(newView:)
//            forControlEvents:UIControlEventTouchUpInside];
//        
//        self.restuarants = [[UIButton alloc] initWithFrame:CGRectMake(91, 0, 86, 41)];
//        self.restuarants.tag = 2;
//        [self.restuarants setBackgroundImage:[UIImage imageNamed:@"375Restaurants.png"] forState:UIControlStateNormal];
//        [self.restuarants addTarget:self
//                             action:@selector(newView:)
//                   forControlEvents:UIControlEventTouchUpInside];
//        
//        self.record = [[UIButton alloc] initWithFrame:CGRectMake(177, 0, 93, 41)];
//        self.record.tag = 3;
//        [self.record setBackgroundImage:[UIImage imageNamed:@"375Record.png"] forState:UIControlStateNormal];
//        [self.record addTarget:self
//                        action:@selector(newView:)
//              forControlEvents:UIControlEventTouchUpInside];
//        
//        self.map = [[UIButton alloc] initWithFrame:CGRectMake(270, 0, 105, 41)];
//        self.map.tag = 4;
//        [self.map setBackgroundImage:[UIImage imageNamed:@"375Map.png"] forState:UIControlStateNormal];
//        [self.map addTarget:self
//                     action:@selector(newView:)
//           forControlEvents:UIControlEventTouchUpInside];
//        
//        [self.buttonsContainer addSubview:self.home];
//        [self.buttonsContainer addSubview:self.restuarants];
//        [self.buttonsContainer addSubview:self.record];
//        [self.buttonsContainer addSubview:self.map];
//        
//        [self.view addSubview:self.buttonsContainer];
//    }
//    
//    if (screenWidth == 414) {
//        self.buttonsContainer = [[UIView alloc] init];
//        self.buttonsContainer.tag = 68;
//        self.buttonsContainer.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-45, 414, 45);
//        self.buttonsContainer.backgroundColor = [UIColor clearColor];
//        
//        self.home = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 45)];
//        self.home.tag = 1;
//        [self.home setBackgroundImage:[UIImage imageNamed:@"414Home.png"] forState:UIControlStateNormal];
//        [self.home addTarget:self
//                      action:@selector(newView:)
//            forControlEvents:UIControlEventTouchUpInside];
//        
//        self.restuarants = [[UIButton alloc] initWithFrame:CGRectMake(100, 0, 95, 45)];
//        self.restuarants.tag = 2;
//        [self.restuarants setBackgroundImage:[UIImage imageNamed:@"414Restaurants.png"] forState:UIControlStateNormal];
//        [self.restuarants addTarget:self
//                             action:@selector(newView:)
//                   forControlEvents:UIControlEventTouchUpInside];
//        
//        self.record = [[UIButton alloc] initWithFrame:CGRectMake(195, 0, 103, 45)];
//        self.record.tag = 3;
//        [self.record setBackgroundImage:[UIImage imageNamed:@"414Record.png"] forState:UIControlStateNormal];
//        [self.record addTarget:self
//                        action:@selector(newView:)
//              forControlEvents:UIControlEventTouchUpInside];
//        
//        self.map = [[UIButton alloc] initWithFrame:CGRectMake(298, 0, 116, 45)];
//        self.map.tag = 4;
//        [self.map setBackgroundImage:[UIImage imageNamed:@"414Map.png"] forState:UIControlStateNormal];
//        [self.map addTarget:self
//                     action:@selector(newView:)
//           forControlEvents:UIControlEventTouchUpInside];
//        
//        [self.buttonsContainer addSubview:self.home];
//        [self.buttonsContainer addSubview:self.restuarants];
//        [self.buttonsContainer addSubview:self.record];
//        [self.buttonsContainer addSubview:self.map];
//        
//        [self.view addSubview:self.buttonsContainer];
//    }
//    
//    if (screenWidth > 414) {
        self.buttonsContainer = [[UIView alloc] init];
        self.buttonsContainer.tag = 68;
        self.buttonsContainer.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width - 300) / 2, [[UIScreen mainScreen] bounds].size.height-45, 300, 45);
        self.buttonsContainer.backgroundColor = [UIColor clearColor];
        
        self.home = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 45)];
        self.home.tag = 1;
        [self.home setBackgroundImage:[UIImage imageNamed:@"414Home.png"] forState:UIControlStateNormal];
        [self.home addTarget:self
                      action:@selector(newView:)
            forControlEvents:UIControlEventTouchUpInside];
        
        self.restuarants = [[UIButton alloc] initWithFrame:CGRectMake(100, 0, 95, 45)];
        self.restuarants.tag = 2;
        [self.restuarants setBackgroundImage:[UIImage imageNamed:@"414Restaurants.png"] forState:UIControlStateNormal];
        [self.restuarants addTarget:self
                             action:@selector(newView:)
                   forControlEvents:UIControlEventTouchUpInside];
        
        self.record = [[UIButton alloc] initWithFrame:CGRectMake(195, 0, 103, 45)];
        self.record.tag = 3;
        [self.record setBackgroundImage:[UIImage imageNamed:@"414Record.png"] forState:UIControlStateNormal];
        [self.record addTarget:self
                        action:@selector(newView:)
              forControlEvents:UIControlEventTouchUpInside];
    
    
        self.map = [[UIButton alloc] initWithFrame:CGRectMake(195, 0, 103, 45)];
//        self.map = [[UIButton alloc] initWithFrame:CGRectMake(298, 0, 116, 45)];
        self.map.tag = 4;
        [self.map setBackgroundImage:[UIImage imageNamed:@"414Map.png"] forState:UIControlStateNormal];
        [self.map addTarget:self
                     action:@selector(newView:)
           forControlEvents:UIControlEventTouchUpInside];
        
        [self.buttonsContainer addSubview:self.home];
        [self.buttonsContainer addSubview:self.restuarants];
//        [self.buttonsContainer addSubview:self.record];
        [self.buttonsContainer addSubview:self.map];
        
        [self.view addSubview:self.buttonsContainer];
//    }

}

- (void) newView: (id) sender {
    UIButton *clicked = (UIButton *) sender;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if (clicked.tag == 1) {
        SecondViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SecondViewController"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    if (clicked.tag == 2) {
        RestaurantsViewController *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"RestaurantsViewController"];
        [self presentViewController:vc1 animated:NO completion:nil];
    }
    if (clicked.tag == 3) {
        ViewController *vc2 = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [self presentViewController:vc2 animated:NO completion:nil];
    }
    if (clicked.tag == 4) {
        MapViewController *vc3 = [storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
        [self presentViewController:vc3 animated:NO completion:nil];
    }
}

- (void) loadPins
{
    /*
    NSArray *json = [[NSArray alloc] init];
    
    NSURL * url = [[NSURL alloc] initWithString:@"http://209.109.119.123:830/MapData.asmx?op=HelloWorld"];
    NSData* data = [NSData dataWithContentsOfURL: url];
    NSError *error;
    
    if (data)
        json = [[NSArray alloc] initWithArray:[NSJSONSerialization
                                               JSONObjectWithData:data
                                               options:kNilOptions
                                               error:&error]];
    
    NSLog(@"WebService results: [%@]", json);
    */
    NSData *mydata = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"https://app.schoolicious.org/SR_RestMapData.aspx"]];
//    NSData *mydata = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://208.109.119.123:830/SR_RestMapData.aspx"]];
    NSString *mybody = [[NSString alloc] initWithData:mydata encoding:NSUTF8StringEncoding];
    NSArray  *arr = [mybody componentsSeparatedByString:@"MyData>"];
    NSString *wrk = [arr[1] substringToIndex:[arr[1] length]-2];
    
    NSArray *wrk1 = [wrk componentsSeparatedByString:@"^"];
    int i;
    for (i = 1; i < [wrk1 count]; i++)
    {
        NSArray *wrk2 = [wrk1[i] componentsSeparatedByString:@","];
        NSString *subtitle = wrk2[0];
        NSString *title = wrk2[1];
        double lat = [wrk2[2] doubleValue];
        double lng = [wrk2[3] doubleValue];
        NSString *rID = wrk2[4];
        [self AddAnnotation:lat  second:lng  third:title  fourth:subtitle url:rID];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
