//
//  ViewController.h
//  QRCodeReader
//
//  Created by Gabriel Theodoropoulos on 27/11/13.
//  Copyright (c) 2013 Gabriel Theodoropoulos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>



@interface ViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate, UITextFieldDelegate>



@property (weak, nonatomic) IBOutlet UIView *viewPreview;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bbitemStart;
@property (strong, nonatomic) IBOutlet UIView *bottomBar;
@property (strong, nonatomic) IBOutlet UIButton *scanLabel;
@property (strong, nonatomic) IBOutlet UIView *buttonsContainer;

@property (strong, nonatomic) UIButton *home;
@property (strong, nonatomic) UIButton *restuarants;
@property (strong, nonatomic) UIButton *map;
@property (strong, nonatomic) UIButton *toolBar;
@property (strong, nonatomic) UIButton *record;

//deleted web view cause it wasn't used
//@property (weak, nonatomic) IBOutlet UIWebView *entryPage;

- (IBAction)startStopReading:(id)sender;

- (void)webViewDidFinishLoad:(UIWebView *)webView;

@end
